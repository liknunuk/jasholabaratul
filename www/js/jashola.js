var tanggale = [];
        
async function loadjson(kota){
    // const jadwal =  await fetch('./js/jadwal.json');
    // let jamsho = await jadwal.json();
    let jamsho = jadwal;
    let jaturi =[];
    let jasota =[];
    let harike =[];
    for( let k = 0 ; k < kota.length ; k++ ){
        let city = kota[k];
        for( let i = 0 ; i < 7 ; i++){
            var date = new Date();
            date.setDate(date.getDate() + i);
            let month = (date.getMonth() + 1).toString().padStart(2,'0');
            let day = date.getDate().toString().padStart(2,'0');
            harike[i]=day+'/'+month;
            tanggale['year']=date.getFullYear();
            let el = city+month+'-'+day;
            jaturi[i]=jamsho[el];
            // console.log(city,jamsho[0][el]);
        }
        tanggale['buri']=harike;
        jasota[city]=jaturi;
        jaturi=[];
    }
    return jasota;
}

function init(){
    makeTable();
    kota = ['banjar-','bantul-'];
    const jsbanj = loadjson(kota)
    .then((data)=>{
        showJadwal(data);
        // console.log(data);
    });        
}

function showJadwal(data){
    // console.log(tanggale);
    for(let i =0 ; i < data['banjar-'].length; i++){
        $(`#tanggal_${i}`).text(tanggale['buri'][i]+'/'+tanggale['year']);
        $(`#bj-${i}-ims`).text(data['banjar-'][i].imsyak);
        $(`#bj-${i}-shu`).text(data['banjar-'][i].shubuh);
        $(`#bj-${i}-ter`).text(data['banjar-'][i].terbit);
        $(`#bj-${i}-dhu`).text(data['banjar-'][i].dhuha);
        $(`#bj-${i}-dzu`).text(data['banjar-'][i].dzuhur);
        $(`#bj-${i}-asy`).text(data['banjar-'][i].ashr);
        $(`#bj-${i}-mag`).text(data['banjar-'][i].maghrib);
        $(`#bj-${i}-isy`).text(data['banjar-'][i].isya);

        $(`#bt-${i}-ims`).text(data['bantul-'][i].imsyak);
        $(`#bt-${i}-shu`).text(data['bantul-'][i].shubuh);
        $(`#bt-${i}-ter`).text(data['bantul-'][i].terbit);
        $(`#bt-${i}-dhu`).text(data['bantul-'][i].dhuha);
        $(`#bt-${i}-dzu`).text(data['bantul-'][i].dzuhur);
        $(`#bt-${i}-asy`).text(data['bantul-'][i].ashr);
        $(`#bt-${i}-mag`).text(data['bantul-'][i].maghrib);
        $(`#bt-${i}-isy`).text(data['bantul-'][i].isya);
    }

}

function makeTable(){
    for( let i = 0 ; i < 7 ; i++ ){
        let tbel = `
                <tr><th colspan="3" id="tanggal_${i}" class='subheader'>Tanggal</th></tr>
                <tr><td>Imsyak</td><td id='bj-${i}-ims'>0</td><td id='bt-${i}-ims'>0</td></tr>
                <tr><td>Shubuh</td><td id='bj-${i}-shu'>0</td><td id='bt-${i}-shu'>0</td></tr>
                <tr><td>Terbit</td><td id='bj-${i}-ter'>0</td><td id='bt-${i}-ter'>0</td></tr>
                <tr><td>Dhuha</td><td id='bj-${i}-dhu'>0</td><td id='bt-${i}-dhu'>0</td></tr>
                <tr><td>Dzuhur</td><td id='bj-${i}-dzu'>0</td><td id='bt-${i}-dzu'>0</td></tr>
                <tr><td>Asyar</td><td id='bj-${i}-asy'>0</td><td id='bt-${i}-asy'>0</td></tr>
                <tr><td>Maghrib</td><td id='bj-${i}-mag'>0</td><td id='bt-${i}-mag'>0</td></tr>
                <tr><td>Isya</td><td id='bj-${i}-isy'>0</td><td id='bt-${i}-isy'>0</td></tr>
        `;
        $("#prayerTimes").append(tbel);
    }
}

document.addEventListener('deviceready', init, false);
document.addEventListener('DOMContentLoaded',init,false);
$("#refresh").on("click",function(){
    location.reload();
})