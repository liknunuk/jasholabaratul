const namabulan = {
    "01":"Januari",
    "02":"Februari",
    "03":"Maret",
    "04":"April",
    "05":"Mei",
    "06":"Juni",
    "07":"Juli",
    "08":"Agustus",
    "09":"September",
    "10":"Oktober",
    "11":"Nopember",
    "12":"Desember"
};

$().ready( function(){
    // cek tanggal hari ini
    let today = todayIs();
    // console.log(today);
    $("#today").html(today.today);
    // tanggal sepekan ke depan
    let tgSepekan=sepekanMendatang();
    for( let t=0 ; t < tgSepekan.tanggal.length; t++){
        let ibna = 'banjar-'+tgSepekan.tanggal[t];
        let ibtl = 'bantul-'+tgSepekan.tanggal[t];
        let tgl = `${tgSepekan.tahun[t]}-${tgSepekan.tanggal[t]}`;
        let blokJadwal=`
        <tr class='tanggal'>
          <td>Tanggal</td>
          <td colspan="2">${pecahtanggal(tgl)}</td>
        </tr>
        <tr class='tbhead'>
            <td>Waktu</td><td>Bantul, DIY</td><td>Banjarnegara</td></tr>
        </tr>
        <tr class='jamsho'><td>Imsyak</td><td>${jadwal[ibtl].imsyak}</td><td>${jadwal[ibna].imsyak}</td></td>
        <tr class='jamsho'><td>Shubuh</td><td>${jadwal[ibtl].shubuh}</td><td>${jadwal[ibna].shubuh}</td></td>
        <tr class='jamsho'><td>Terbit</td><td>${jadwal[ibtl].terbit}</td><td>${jadwal[ibna].terbit}</td></td>
        <tr class='jamsho'><td>Dhuha</td><td>${jadwal[ibtl].dhuha}</td><td>${jadwal[ibna].dhuha}</td></td>
        <tr class='jamsho'><td>Dzuhur</td><td>${jadwal[ibtl].dzuhur}</td><td>${jadwal[ibna].dzuhur}</td></td>
        <tr class='jamsho'><td>Ashar</td><td>${jadwal[ibtl].ashr}</td><td>${jadwal[ibna].ashr}</td></td>
        <tr class='jamsho'><td>Maghrib</td><td>${jadwal[ibtl].maghrib}</td><td>${jadwal[ibna].maghrib}</td></td>
        <tr class='jamsho'><td>Isya</td><td>${jadwal[ibtl].isya}</td><td>${jadwal[ibna].isya}</td></td>
        <tr class='blank'><td colspan="3">&nbsp;</td></tr>
        `;
        $("#jadwal").append(blokJadwal);
    }
})

function todayIs(){
    var date = new Date();
    // Pecah jadi tahun,bulan,hari,jam,menit,detik
    let tahun,bulan,hari,jam,menit,detik;
    tahun = date.getFullYear().toString();
    bulan = (date.getMonth() + 1).toString().padStart(2,'0');
    hari  = date.getDate().toString().padStart(2,'0');
    jam   = date.getHours().toString().padStart(2,'0');
    menit = date.getMinutes().toString().padStart(2,'0');
    detik = date.getSeconds().toString().padStart(2,'0');
    let today = hari + " " + namabulan[bulan] + " " + tahun + ", "+jam+":"+menit+":"+detik;
    let output = {
        'today':today,
        'tahun':tahun,
        'bulan':bulan,
        'hari' :hari
    }
    return output;
}

function sepekanMendatang(){
    var tanggalSepekan=[];
    var tahunBerlaku=[];
    for( let i = 0 ; i < 7 ; i++){
        var date = new Date();
        date.setDate(date.getDate() + i);
        // ambil tanggal dan bulannya saja
        let tgl = date.getDate().toString().padStart(2,0);
        let bln = (date.getMonth()+1).toString().padStart(2,0);
        let thn = (date.getFullYear()).toString();
        tanggalSepekan.push(`${bln}-${tgl}`);
        tahunBerlaku.push(thn);
    }
    // console.log(tanggalSepekan);
    let output = {'tanggal':tanggalSepekan,'tahun':tahunBerlaku};
    return output;
}

function pecahtanggal(tanggal){
    tbh = tanggal.split("-");
    bl = tbh[1];
    bln = namabulan[bl];
    return `${tbh[2]} ${bln} ${tbh[0]}`;
}