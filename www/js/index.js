/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

// Wait for the deviceready event before using any of Cordova's device APIs.
// See https://cordova.apache.org/docs/en/latest/cordova/events/events.html#deviceready
document.addEventListener('deviceready', onDeviceReady, false);

function onDeviceReady() {
    // Cordova is now initialized. Have fun!

    // console.log('Running cordova-' + cordova.platformId + '@' + cordova.version);
    // document.getElementById('deviceready').classList.add('ready');
    
}
const today = new Date();
var day = today.getDate();
var month = today.getMonth() + 1;
var year = today.getFullYear();

$().ready(function(){

    // initiate current date
    // const today = new Date();
    // let day = today.getDate();
    // let month = today.getMonth() + 1;
    // let year = today.getFullYear();
    
    var bulan = month.toString().padStart(2,0);
    var yearMonth = year +"-"+bulan;
    var isSetData , isSamePeriod;
    var period = localStorage.getItem('period');
    

    if(localStorage.getItem('bantul') === null && localStorage.getItem('banjar') === null){
        isSetData = false;
    }else{
        isSetData = true;
    }
    
    if( period === yearMonth){
        isSamePeriod = true;
    }else{
        isSamePeriod = false;
    }
    
    // Pengambilan data dilakukan jika
    // isSetData === false atau isSamePeriod === false
    if( isSetData === false || isSamePeriod == false){
        
        localStorage.setItem('period',year+'-'+bulan);
        getData(year,bulan);
        setInterval(readData(day),3000);
        // console.log('procedure 1 executed');
        location.reload();
    }

    // Pembacaan data dilakukan bila
    // isSetData === true dan isSamePeriod === true
    if( isSetData === true && isSamePeriod === true){
        const delay = setTimeout(readData(day),2000);
        // console.log('procedure 2 executed');
    }
})

$("#refresh").on("click",function(){
    // console.log('refresh data')
    // getData(year,month);
    alert('reload');
    location.reload();
})

function getData(tahun,bulan){
        getJadwal('bantul',tahun,bulan).then(response=>{
            localStorage.setItem('bantul',response);
        });
        getJadwal('banjarnegara',tahun,bulan).then(response=>{
            localStorage.setItem('banjar',response);
        });
        // console.log("All data set");
}
async function getJadwal(kota,tahun,bulan){
    let url = `https://raw.githubusercontent.com/lakuapik/jadwalsholatorg/master/adzan/${kota}/${tahun}/${bulan}.json`;
    let obj = await fetch(url);
    let rsp = await obj.text();
    return rsp;
}

function readData(day){
    // console.log('day:',day);
    let jbantul = localStorage.getItem('bantul');
    let bantul = JSON.parse(jbantul);
    let jbanjar = localStorage.getItem('banjar');
    let banjar = JSON.parse(jbanjar);
    // console.log('All data read');
    let ulang = 0;
    for (i in bantul) {
        
        let tanggal = puterTanggal(bantul[i].tanggal);
        let tg = parseInt(i) + 1;
        // console.log('tg:',tg);
        if( tg >= day ){

            $("#prayerTimes").append(`
            <tr>
                <td colspan='3' style='text-align:center; background-color:gray; color: white; font-weight:bold;'>
                    ${tanggal}
                </td>
            </tr>
            <tr><td>Imsyak</td><td>${bantul[i].imsyak}</td><td>${banjar[i].imsyak}</td></tr>
            <tr><td>Shubuh</td><td>${bantul[i].shubuh}</td><td>${banjar[i].shubuh}</td></tr>
            <tr><td>Dzuhur</td><td>${bantul[i].dzuhur}</td><td>${banjar[i].dzuhur}</td></tr>
            <tr><td>Asyar</td><td>${bantul[i].ashr}</td><td>${banjar[i].ashr}</td></tr>
            <tr><td>Maghrib</td><td>${bantul[i].magrib}</td><td>${banjar[i].magrib}</td></tr>
            <tr><td>Isya</td><td>${bantul[i].isya}</td><td>${banjar[i].isya}</td></tr>
            `); 
            ulang+=1;
            if( ulang >= 7){ break; }
        }
    }

    // location.reload();
}

function puterTanggal(tgl){
    t=tgl.split('-');
    return `${t[2]}-${t[1]}-${t[0]}`;
}
/*
tanggal:"2023-12-29"
imsyak:"03:50"
shubuh:"04:00"
terbit:"05:22"
dhuha:"05:46"
dzuhur:"11:43"
ashr:"15:10"
magrib:"18:00"
isya:"19:16"
*/
